export interface ApplicationConfig {
    urlApi: {
        pandascoreDev: string,
        pandascoreProd: string,
        token: string
    };
}

export const CONFIG: ApplicationConfig = {
    urlApi: {
        pandascoreDev: 'http://127.0.0.1:9001/',
        pandascoreProd: 'http://ec2-35-181-117-93.eu-west-3.compute.amazonaws.com/',
        token: 'qPmmA12az..//A**3edErTmLéé3..jjKilOpqDfEr123£9°LL980OPM'
    }
};