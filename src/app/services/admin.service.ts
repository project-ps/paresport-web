import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Admin } from '../models/admin';
import { CONFIG } from 'src/app.config';

@Injectable({ providedIn: 'root' })
export class AdminService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Admin[]>(`${CONFIG.urlApi.pandascoreProd}admins`);
    }
}