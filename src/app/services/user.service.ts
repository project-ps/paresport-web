import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/user';
import { CONFIG } from 'src/app.config';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${CONFIG.urlApi.pandascoreProd}users`);
    }

    delete(id) {
        return this.http.delete<User[]>(`${CONFIG.urlApi.pandascoreProd}user/${id}`);
    }
}