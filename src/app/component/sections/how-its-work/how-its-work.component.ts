import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-how-its-work',
  templateUrl: './how-its-work.component.html',
  styleUrls: ['./how-its-work.component.css']
})
export class HowItsWorkComponent implements OnInit {

  @Input() title: string;
  @Input() text: string;
  @Input() img: string;
  @Input() imgWidth: string;
  @Input() ltr: boolean;

  constructor() { }

  ngOnInit() {
  }

}
