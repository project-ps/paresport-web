export class User {
    _id: string;
    kcId: string;
    firstName: string;
    lastName: string;
    surname: string;
    mail: string;
    phone: string;
    localisation: string;
    tokens: number;
    betsId: number[];
}