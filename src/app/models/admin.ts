export class Admin {
    id: number;
    email:string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
}